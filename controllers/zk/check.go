package zk

import (
	"git.oschina.net/tinkler/cloudshop/controllers"
	"git.oschina.net/tinkler/cloudshop/models/zktimes"
)

//CheckController implement ZKRouter 考勤控制器
type CheckController struct {
	controllers.ZKBase
}

//URLMapping mapping the url
func (cc *CheckController) URLMapping() {
	cc.Mapping("Index", cc.Index)
}

//Index of CheckController
//@router /check/index [get]
func (cc *CheckController) Index() {
	users := zktimes.GetUsers()
	cc.TplName = `zk/check/index.tpl`
	cc.Data[`users`] = &users
}
