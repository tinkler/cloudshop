package main

import (
	_ "git.oschina.net/tinkler/cloudshop/routers"

	"log"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

func init() {
	orm.RegisterDriver("mysql", orm.DRMySQL)
	log.Print("ee")
	orm.RegisterDataBase("default", "mysql", "tinkler:136132@tcp(rds6jrve367nmfe.mysql.rds.aliyuncs.com:3306)/spaceshop?charset=utf8", 30)
}

func main() {
	beego.Run()
}
