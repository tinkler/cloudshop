
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bootswatch: Cerulean</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="/static/css/bootstrap-blue.css" media="screen">
    <link rel="stylesheet" href="/static/css/bootstrap-blue-custom.css" media="screen">

    </script>
  </head>
  <body>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="./index" class="navbar-brand">考勤管理系统</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">

          </ul>

          <ul class="nav navbar-nav navbar-right">
            
          </ul>

        </div>
      </div>
    </div>

    <div class="container">
        <!-- Tables
      ================================================== -->
      <div class="bs-docs-section">

        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h2 id="tables">排班表</h2>
            </div>
            <div class="col-lg-12">
              <form class="form-inline">
                <div class="form-group">
                  <label for="check_month">月份</label>
                  <input type="text" class="form-control" id="check_month" placeholder="5">
                </div>
                <button type="submit" class="btn btn-default">查询</button>
              </form>
            </div>
            <div class="bs-component">
              <table class="table table-striped table-hover ">
                <thead>
                  <tr>
                    <th style="min-width:65px">姓名</th>
                    <th>组</th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>13</th>
                    <th>14</th>
                    <th>15</th>
                    <th>16</th>
                    <th>17</th>
                    <th>18</th>
                    <th>19</th>
                    <th>20</th>
                    <th>21</th>
                    <th>22</th>
                    <th>23</th>
                    <th>24</th>
                    <th>25</th>
                    <th>26</th>
                    <th>27</th>
                    <th>28</th>
                    <th>29</th>
                    <th>30</th>
                    <th>31</th>
                  </tr>
                </thead>
                <tbody id="users">
                </tbody>
              </table> 
            </div><!-- /example -->
          </div>
        </div>
      </div>
    </div>
    
    <script src="/static/js/jquery-1.10.2.min.js"></script>
    <script src="/static/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      function checkAM(user,needDate){
        var html = ''
        var checkInUpAM = 0
        var checkInDownAM = 0
        var checkInUpPM = 0
        var checkInDownPM = 0
        var tipHtml = ''
        for(var lday in user.Checks){
          if(Date.parse(lday) == needDate.getTime()){
            var checkDateTimes = user.Checks[lday]
            for(var k in checkDateTimes){
              var checkDateTime = Date.parse(checkDateTimes[k])
              tipHtml += checkDateTimes[k] + '>>>'
              var nowDateTime = needDate.getTime()
              //区别生鲜早班
              var amTime = 8
              if(user.Dept == 5){
                amTime = 7
              }
              if(checkDateTime < nowDateTime + 3600000 * amTime && checkDateTime > nowDateTime){
                checkInUpAM = 1
              }else if(checkDateTime < nowDateTime + 3600000 * 17 && checkDateTime > nowDateTime + 3600000 * 12){
                checkInDownAM = 1
              }else if(checkDateTime < nowDateTime + 3600000 * 18){
                checkInUpPM = 1
              }else if(checkDateTime < nowDateTime + 3600000 * 23.9 && checkDateTime > nowDateTime + 3600000 * 22.5){
                checkInDownPM = 1
              }
            }
          }
          
        }
        var haveTimes = checkInUpAM + checkInDownAM + checkInUpPM + checkInDownPM
        if(haveTimes == 4){
          html += '<td style="background-color:lightgreen">早</td>'
        }else if(haveTimes == 0){
          html += '<td style="background-color:lightcoral">休</td>'
        }else{
          html += '<td style="background-color:lightsalmon" data-toggle="tooltip" title="'+ tipHtml + '">早</td>'
        }

        return html
      }
      function checkPM(user,needDate){
        var html = ''
        var checkInUp = 0
        var checkInDown = 0
        var tipHtml = ''
        for(var lday in user.Checks){
          if(Date.parse(lday) == needDate.getTime()){
            var checkDateTimes = user.Checks[lday]
            for(var k in checkDateTimes){
              var checkDateTime = Date.parse(checkDateTimes[k])
              tipHtml += checkDateTimes[k] + '>>>'
              var nowDateTime = needDate.getTime()
              if(checkDateTime < nowDateTime + 3600000 * 12 && checkDateTime > nowDateTime){
                checkInUp = 1
              }else if(checkDateTime > nowDateTime + 3600000 * 18){
                checkInDown = 1
              }
            }
          }
        }
        var haveTimes = checkInUp + checkInDown
        if(haveTimes == 2){
          html += '<td style="background-color:lightgreen">中</td>'
        }else if(haveTimes == 0){
          html += '<td style="background-color:lightcoral">休</td>'
        }else{
          html += '<td style="background-color:lightsalmon" title="' + tipHtml + '">中</td>'
        }
        return html
      }

      $.ajax({
        url: '/zk/as/results',
        data: 'year=2016',
        success:function(users){
          for(var i in users){
            var html = '<tr>'
            html += '<td id="user_' + users[i].UserID + '">' + users[i].UserName + '</td>'
            var groupName = 'G'
            if(users[i].Dept == 2){
              groupName = 'A'
            }else if(users[i].Dept == 4){
              groupName = 'B'
            }else if(users[i].Dept == 5){
              groupName = 'C'
            }else if(users[i].Dept == 6){
              groupName = 'D'
            }else if(users[i].Dept == 7){
              groupName = 'E'
            }else if(users[i].Dept == 8){
              groupName = 'F'
            }
            html += '<td>' + groupName + '</td>'
            for(var day=1;day<=30;day++){
              var needDate = new Date()
              needDate.setFullYear(2016)
              needDate.setMonth(11)//设置月份
              needDate.setDate(day)
              needDate.setHours(0,0,0,0)
              if(users[i].Dept == 2 || users[i].Dept == 5 || users[i].Dept == 7){
                if(day%2 == 1){
                  html += checkAM(users[i],needDate)
                }else{
                  html += checkPM(users[i],needDate)
                }
              }else{
               if(day%2 == 0){
                  html += checkAM(users[i],needDate)
                }else{
                  html += checkPM(users[i],needDate)
                }
              }
            }
            html += '</tr>'
            $("#users").append(html)
          }
        }
      })

      
    </script>
</html>
