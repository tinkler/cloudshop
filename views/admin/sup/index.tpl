<div class="bs-docs-section">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h2>供应商列表</h2>
            </div>
            <div class="bs-component">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>供应商编码</th>
                            <th>供应商名称</th>
                            <th>所属地区</th>
                            <th>供应商联系人</th>
                            <th>供应商联系方式</th>
                            <th>供应商银行开户名</th>
                            <th>供应商银行账号</th>
                            <th>对账明细</th>
                            <!--<th>特价情况</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        {{range $k,$info := .SupcustInfo}}
                        <tr>
                            <td>{{$info.SupcustNo}}</td>
                            <td>{{$info.SupName}}</td>
                            <td>{{$info.RegionName}}</td>
                            <td>{{$info.SupMan}}</td>
                            <td>{{$info.SupTel}}</td>
                            <td>{{$info.SupCashBankMan}}</td>
                            <td>{{$info.SupCashBankNo}}</td>
                            <td>
                                <a href="/admin/sup/pay_list?supcust_no={{$info.SupcustNo}}">查看</a>
                                
                            </td>
                            <!--<td>
                                <a href="/admin/sup/spe_sold">检索特价</a>
                            </td>-->
                        </tr>
                        {{end}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>