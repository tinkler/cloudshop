<div class="bs-docs-section">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h2>{{with $info := (index .PaymentList 0)}} {{$info.Supcust.SupName}}{{end}}付款列表</h2>
            </div>
            <div class="bs-component">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>付款单据编号</th>
                            <th>操作人</th>
                            <th>操作日期</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{range $k,$info := .PaymentList}}
                        <tr>
                            <td>{{$info.SheetNo}}</td>
                            <td>{{$info.OperName}}</td>
                            <td>{{dateformat $info.OperDate "2006-01-02"}}</td>
                            <td>
                                <a href="/admin/sup/pay_detail?sheet_no={{$info.SheetNo}}">查看</a>
                                
                            </td>
                        </tr>
                        {{end}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
            <a class="btn btn-sm btn-default" href="/admin/sup/index">返回供应商列表</a>
        </div>
    </div>
</div>