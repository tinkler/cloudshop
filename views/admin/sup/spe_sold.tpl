<div class="row">
    <div class="col-lg-12">
        <form class="form-inline">
            <div class="form-group">
                <label for="start_date">开始日期</label>
                <input type="text" class="form-control input-sm" id="start_date" placeholder="2016-01-02" />
            </div>
            <div class="form-group">
                <label for="end_date">结束日期</label>
                <input type="text" class="form-control input-sm" id="end_date" placeholder="2016-01-31" />
            </div>
            <button class="btn btn-default btn-sm">检索</button>
            <a href="/admin/sup/index" class="btn btn-default btn-sm">返回</a>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-stripd">
            <thead>
                <tr>
                    <th>商品编码</th>
                    <th>国际条码</th>
                    <th>单位</th>
                    <th>原共价</th>
                    <th>原售价</th>
                    <th>特供价</th>
                    <th>特售价</th>
                    <th>开始</th>
                    <th>结束</th>
                    <th>特售数量</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
