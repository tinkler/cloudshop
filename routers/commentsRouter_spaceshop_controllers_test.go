package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["spaceshop/controllers/test:DartController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/test:DartController"],
		beego.ControllerComments{
			"Index",
			`/dart/index`,
			[]string{"get"},
			nil})

}
