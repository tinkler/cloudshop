package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["spaceshop/controllers/sup:ReconciliationController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/sup:ReconciliationController"],
		beego.ControllerComments{
			"Index",
			`/reconciliation/index`,
			[]string{"get"},
			nil})

}
