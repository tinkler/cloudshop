package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:StoController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:StoController"],
		beego.ControllerComments{
			Method: "Check",
			Router: `/sto/check`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"],
		beego.ControllerComments{
			Method: "Index",
			Router: `/sup/index`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"],
		beego.ControllerComments{
			Method: "Fee",
			Router: `/sup/fee`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"],
		beego.ControllerComments{
			Method: "PayList",
			Router: `/sup/pay_list`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"],
		beego.ControllerComments{
			Method: "PayDetail",
			Router: `/sup/pay_detail`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"],
		beego.ControllerComments{
			Method: "SaveFee",
			Router: `/sup/save_fee`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"] = append(beego.GlobalControllerRouter["git.oschina.net/tinkler/cloudshop/controllers/admin:SupController"],
		beego.ControllerComments{
			Method: "SpeSold",
			Router: `/sup/spe_sold`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

}
