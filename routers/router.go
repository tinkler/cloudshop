package routers

import (
	"git.oschina.net/tinkler/cloudshop/controllers/test"
	"git.oschina.net/tinkler/cloudshop/controllers/zk"

	"git.oschina.net/tinkler/cloudshop/controllers/admin"
	"git.oschina.net/tinkler/cloudshop/controllers/sup"

	"git.oschina.net/tinkler/cloudshop/controllers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/session"
)

func init() {
	sessionConf := &session.ManagerConfig{
		CookieName:      "cloudshopsession",
		EnableSetCookie: true,
		Gclifetime:      3600,
		Maxlifetime:     3600,
		Secure:          false,
		CookieLifeTime:  3600,
		ProviderConfig:  "",
	}
	// `{"cookieName":"spaceshopsession", "enableSetCookie,omitempty": true, "gclifetime":3600, "maxLifetime": 3600, "secure": false, "sessionIDHashFunc": "sha1", "sessionIDHashKey": "136132", "cookieLifeTime": 3600, "providerConfig": ""}`
	globalSessions, _ := session.NewManager("memory", sessionConf)
	go globalSessions.GC()
	adminNS := beego.NewNamespace("/admin",
		beego.NSRouter("/login", &controllers.AdminBase{}, "*:Login"),
		beego.NSInclude(
			&admin.SupController{},
			&admin.StoController{},
		),
	)
	supplierNS := beego.NewNamespace("/supplier",
		beego.NSInclude(
			&sup.ReconciliationController{},
		),
	)
	zkNS := beego.NewNamespace("/zk",
		beego.NSInclude(
			&zk.CheckController{},
			&zk.ArrangeSchedualController{},
		),
	)
	testNS := beego.NewNamespace("/test",
		beego.NSInclude(
			&test.DartController{},
		),
	)
	beego.AddNamespace(adminNS)
	beego.AddNamespace(testNS)
	beego.AddNamespace(supplierNS)
	beego.AddNamespace(zkNS)
}
