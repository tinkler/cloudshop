package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["spaceshop/controllers/zk:ArrangeSchedualController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/zk:ArrangeSchedualController"],
		beego.ControllerComments{
			"Index",
			`/as/index`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["spaceshop/controllers/zk:ArrangeSchedualController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/zk:ArrangeSchedualController"],
		beego.ControllerComments{
			"Results",
			`/as/results`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["spaceshop/controllers/zk:CheckController"] = append(beego.GlobalControllerRouter["spaceshop/controllers/zk:CheckController"],
		beego.ControllerComments{
			"Index",
			`/check/index`,
			[]string{"get"},
			nil})

}
